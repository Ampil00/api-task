
import {
	IsNumber,
	IsString,	
} from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
export interface CommonResponseModel {
	response_code: number;
	response_data: any;
	extra?: string;
}

export class ResponseSuccessMessage {
	@IsString()
	@ApiModelProperty()
	response_code: string;

	@IsString()
	@ApiModelProperty()
	response_data: string;
}

export class ResponseBadRequestMessage {
	@IsNumber()
	@ApiModelProperty()
	status: number;

	@ApiModelProperty()
	errors: Array<string>;

}
export class ResponseErrorMessage {
	@IsNumber()
	@ApiModelProperty()
	status: number;

	@IsString()
	@ApiModelProperty()
	message: string;
}

export enum UserRoles {
	USER = 'USER',
	ADMIN ='ADMIN'
}

