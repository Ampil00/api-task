import { Injectable } from '@nestjs/common';
import { PassportStrategy, AuthGuard } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { createParamDecorator } from '@nestjs/common';
import { UserService } from '../users/users.service';
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(
		private userService: UserService,
	) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			secretOrKey: process.env.SECRET,
		});
		if (!process.env.SECRET) console.log("SECRET not set.");
	}

	// validates user token and returns user's information
	async validate(payload) {
		const { _id } = payload;
		const userInfo = await this.userService.getUserById(_id);
		return userInfo;
	}
}

export const GetUser = createParamDecorator((data, req) => {
	return req.user;
});

