import { Module, } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './users.controller';
import { UserSchema } from './users.model';
import { UserService } from './users.service';
import { AuthService } from '../utils/auth.service';



@Module({
	imports: [
		MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
	],
	controllers: [UserController],
	providers: [UserService, AuthService],
	exports: [UserService, MongooseModule]
})

export class UsersModule {
}
