import * as mongoose from 'mongoose';
import {
	IsNotEmpty,
	IsEmail,
	IsEmpty,
	IsUrl,
	IsNumber,
	Length,
	IsOptional,
	IsPositive,
	Min,
	Equals,
	IsArray,
	ValidateNested,
	IsString,
	Max,
	IsEnum,
	IsAlphanumeric,
	IsBoolean,
} from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { UserRoles } from '../utils/app.model';

export const UserSchema = new mongoose.Schema({
	userName: { type: String },
	email: { type: String, trim: true, lowercase: true, sparse: true },
	password: { type: String },
	salt: { type: String },
	role: { type: String },
	mobileNumber: { type: String },
	address: { 
		street: String, 
		locality: String, 
		city: String, 
		state: String, 
		pincode: String, 
		location: {
			type: {
				type: String,
				default: 'Point',
			},
			coordinates: [],
		}, 
	}
	
}, {
	timestamps: true
});
UserSchema.index({ "address.location": "2dsphere" });

export class LocationDTO{
	@ApiModelProperty()
	coordinates:[]
}
export  class AddressDTO{

	@ApiModelProperty()
	street: String;
	@ApiModelProperty()
	locality: String;
	@ApiModelProperty()
	city: String;
	@ApiModelProperty()
	state: String;
	@ApiModelProperty()
	pincode: String;
	@ApiModelProperty()
	location: LocationDTO  

}
export class UserCreateMobileDTO { 
	_id :string;
	@IsString()
	@ApiModelProperty()
	@IsNotEmpty()
	userName: string;

	@IsString()
	@ApiModelProperty()
	@IsNotEmpty()
	lastName: string;

	@IsString()
	@ApiModelProperty()
	@IsOptional()
	email: string;

	@IsString()
	@ApiModelProperty()
	@IsNotEmpty()
	@Length(6, 35)
	password: string;

	@IsString()
	@IsNotEmpty()
	@ApiModelProperty()
	@Length(8, 15)
	mobileNumber: string;
	salt: string;
	role: string;
    
	@ApiModelProperty()
	address:AddressDTO

}
export class LoginDTO {
	@ApiModelProperty()
	@IsNotEmpty()
	@IsString()
	mobileNumber: string;

	@ApiModelProperty()
	@IsNotEmpty()
	@Length(6, 35)
	@IsString()
	password: string;
}

export class UsersUpdateDTO {
	@IsString()
	@IsOptional()
	@ApiModelProperty()
	userName?: string;

	@IsString()
	@IsOptional()
	@ApiModelProperty()
	lastName?: string;

	@IsString()
	@IsOptional()
	@ApiModelProperty()
	mobileNumber?: string;

	@IsString()
	@IsOptional()
	@IsUrl()
	@ApiModelProperty()
	imageUrl?: string;

	@IsString()
	@IsOptional()
	@ApiModelProperty()
	imageId?: string;

	@IsString()
	@IsOptional()
	@ApiModelProperty()
	filePath?: string;
	playerId?: string;
}

export class CredentialsDTO {
	@IsString()
	@IsNotEmpty()
	@ApiModelProperty()
	email: string;

	@IsString()
	@IsOptional()
	playerId: string

	@IsString()
	@IsNotEmpty()
	@Length(5, 35)
	@ApiModelProperty()
	password: string;
}

export class UserCreateDTO {
	_id:string
	@ApiModelProperty()
	@IsNotEmpty()
	userName?: string;

	@ApiModelProperty()
	@IsNotEmpty()
	mobileNumber?: string;

	
	@ApiModelProperty()
	email: string;

	@IsNotEmpty()
	@ApiModelProperty()
	password?: string

	role?: string

	emailVerificationId: string;

	emailVerificationExpiry: number;

	salt: string;

	emailVerified: boolean;

	@ApiModelProperty()
	address:AddressDTO
}

export class LoginResponseDTO {
	@ApiModelProperty()
	token: string;

	@ApiModelProperty()
	role: string;

	@ApiModelProperty()
	id: string;
}

export class ResponseLogin {
	@IsString()
	@ApiModelProperty()
	response_code: string;

	@ApiModelProperty()
	response_data: LoginResponseDTO;
}

export class AdminUserDTO {
	@ApiModelProperty()
	_id: string;

	@ApiModelProperty()
	status: boolean;

	@ApiModelProperty()
	email: string;

	@ApiModelProperty()
	userName: string;

	@ApiModelProperty()
	lastName: string;

	@ApiModelProperty()
	mobileNumber: string;


	@ApiModelProperty()
	createdAt: string;
}

export class AdminQuery {
	page?: number;
	limit?: number;
}

export enum AdminSettings {
	DEFAULT_PAGE_NUMBER = 1,
	DEFAULT_PAGE_LIMIT = 10,
}

export class UserQuery extends AdminQuery{
	@IsOptional()
	@ApiModelPropertyOptional()
	lat?: number;

	@IsOptional()
	@ApiModelPropertyOptional()
	long?: number;
}