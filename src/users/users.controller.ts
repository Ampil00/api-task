import { Controller, Post, Body, Get, Delete, UseGuards, Param, Put, HttpStatus, Query } from '@nestjs/common';
import {
	UserCreateDTO,
	LoginDTO,
	UsersUpdateDTO, ResponseLogin, AdminQuery, UserQuery,
} from './users.model';
import { UserService } from './users.service';
import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse, ApiImplicitQuery } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { UserRoles, CommonResponseModel, ResponseErrorMessage, ResponseBadRequestMessage, ResponseSuccessMessage } from '../utils/app.model';
import { AuthService } from '../utils/auth.service';
import { GetUser } from '../utils/jwt.strategy';

@Controller('users')
@ApiUseTags('Users')
export class UserController {
	constructor(
		private userService: UserService,
		private authService: AuthService,

	) {
	}

	/* ################################################### NO AUTH ################################## */
	//USER REGISTRATION API
	@Post('/register')
	@ApiOperation({ title: 'Register user' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async registerNewUser(@Body() userData: UserCreateDTO): Promise<CommonResponseModel> {
		try {
			const mobileNumber = this.userService.convertToNumber(userData.mobileNumber);

			if (mobileNumber == 0) return { response_code: HttpStatus.BAD_REQUEST, response_data: "required Valid mobile Number" };
			const checkUser = await this.userService.findUserByEmailOrMobile(userData.email, userData.mobileNumber);
			if (checkUser && checkUser.email == userData.email) return { response_code: HttpStatus.BAD_REQUEST, response_data: "User email already exist" };
			if (checkUser && checkUser.mobileNumber == userData.mobileNumber) return { response_code: HttpStatus.BAD_REQUEST, response_data: "User mobile Number already exist" };
			userData.role = UserRoles.USER;

			const user = await this.userService.createUser(userData);
			if (user) return { response_code: HttpStatus.OK, response_data: "User Registered successfully" };
			else return { response_code: HttpStatus.BAD_REQUEST, response_data: "something went wrong" }
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}
	// USER LOGIN API
	@Post('/login')
	@ApiOperation({ title: 'Log in user' })
	@ApiResponse({ status: 200, description: 'Return user info', type: ResponseLogin })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async validateUser(@Body() credential: LoginDTO): Promise<CommonResponseModel> {
		try {
			const user = await this.userService.getUserByMobileNo(credential.mobileNumber);
			if (!user) return { response_code: HttpStatus.BAD_REQUEST, response_data: "user Mobile number not found" }

			const isValid = await this.authService.verifyPassword(credential.password, user.password);
			if (!isValid) return { response_code: HttpStatus.BAD_REQUEST, response_data: "mobile no or password invalid" }

			const token = await this.authService.generateAccessToken(user._id, user.role);
			return { response_code: HttpStatus.OK, response_data: { token: token, role: user.role, id: user._id } };
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}

	// USER INFOMATION
	@Get('/me')
	@ApiOperation({ title: 'Get logged-in user info' })
	@ApiResponse({ status: 200, description: 'Return user info', type: '' })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async GetUserInfo(@GetUser() user: UserCreateDTO): Promise<CommonResponseModel> {
		try {
			const me = await this.userService.getUserById(user._id);
			if (me) {
				return { response_code: HttpStatus.OK, response_data: me };
			}
			else return { response_code: HttpStatus.OK, response_data: "user Not found" };
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}

	// USER UPDATE OWN PROFILE DETAILS
	@Put('/update/profile')
	@ApiOperation({ title: 'Update profile' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async updateProfile(@GetUser() user: UserCreateDTO, @Body() userInfo: UsersUpdateDTO): Promise<CommonResponseModel> {
		try {

			const response = await this.userService.updateMyInfo(user._id, userInfo);
			if (response) return { response_code: HttpStatus.OK, response_data: response };
			else return { response_code: HttpStatus.BAD_REQUEST, response_data: "something went wrong" }
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}

	//USER DELETE OWN ACCOUNT
	@Delete('/delete/account')
	@ApiOperation({ title: 'Update profile' })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	@UseGuards(AuthGuard('jwt'))
	@ApiBearerAuth()
	public async updateAccount(@GetUser() user: UserCreateDTO): Promise<CommonResponseModel> {
		try {

			const response = await this.userService.deleteUserById(user._id);
			if (response) return { response_code: HttpStatus.OK, response_data: "your Account deleted successfully" };
			else return { response_code: HttpStatus.BAD_REQUEST, response_data: "something went wrong" }
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}

	// ALL USER LIST WITH PAGINATION
	@Get('/all/list')
	@ApiOperation({ title: 'get All User List with pagination' })
	@ApiImplicitQuery({ name: "page", description: "page", required: false, type: Number })
	@ApiImplicitQuery({ name: "limit", description: "limit", required: false, type: Number })
	@ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
	@ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async getAllListOfUser(@GetUser() user: UserCreateDTO, @Query() adminQuery: AdminQuery): Promise<CommonResponseModel> {
		try {
			let pagination = this.userService.getAdminPagination(adminQuery);
			const userData = await Promise.all([
				this.userService.getAllUserList(pagination.page - 1, pagination.limit),
				this.userService.countAllUserList()
			])
			if (userData) return { response_code: HttpStatus.OK, response_data: { data: userData[0], total: userData[1] } };
			else return { response_code: HttpStatus.BAD_REQUEST, response_data: "something went wrong" }
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}
    
	//USER LIST BY SORTING LAT_LONG
	@Get('/list-sort-by-location')
	@ApiOperation({ title: 'Get franchise list for user' })
	@ApiImplicitQuery({ name: "page", description: "page", required: false, type: Number })
	@ApiImplicitQuery({ name: "limit", description: "limit", required: false, type: Number })
	@ApiImplicitQuery({ name: "lat", description: "latitude", required: false, type: Number })
	@ApiImplicitQuery({ name: "long", description: "longitude", required: false, type: Number })
	@ApiResponse({ status: 200, description: 'Return franchise list fo user', type: Number })
	@ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
	public async franchiseNearByList(@GetUser() user: UserCreateDTO,@Query() userQuery: UserQuery): Promise<CommonResponseModel> {
		let query = this.userService.getUserQuery(userQuery);
		try {
			let longLat = [Number(userQuery.long), Number(userQuery.lat)];
			const list = await Promise.all([
				this.userService.getAllUserListByLocation(query.page, query.limit, longLat),
				this.userService.countAllUserList()
			])
	
			return  { response_code: HttpStatus.OK, response_data: { data: list[0], total: list[1] } };
		} catch (e) {
			return { response_code: HttpStatus.INTERNAL_SERVER_ERROR, response_data: e.message }
		}
	}
}
