import { Injectable, } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {UsersUpdateDTO,AdminUserDTO,UserCreateDTO, AdminQuery, AdminSettings, UserQuery} from './users.model';
import { UserRoles } from '../utils/app.model';
import { AuthService } from '../utils/auth.service';

@Injectable()
export class UserService {
	constructor(
		@InjectModel('User') private readonly userModel: Model<any>,
		private authService: AuthService,
	) {

	}

	public async createUser(userData: UserCreateDTO): Promise<UserCreateDTO> {
		if (userData.email) userData.email = userData.email.toLowerCase();
		const { salt, hashedPassword } = await this.authService.hashPassword(userData.password);
		userData.salt = salt;
		userData.password = hashedPassword;
		const user = await this.userModel.create(userData);
		return user;
	}

	public async getUserByMobileNo(mobileNumber: String): Promise<UserCreateDTO> {
		const user = await this.userModel.findOne({ mobileNumber: mobileNumber });
		return user;
	}

	public async updateMyInfo(userId: string, userData: UsersUpdateDTO): Promise<UserCreateDTO> {
		const user = await this.userModel.findByIdAndUpdate(userId, userData);
		return user;
	}

	public async getAllUser(filter: any, page: number, limit: number): Promise<Array<AdminUserDTO>> {
		const skip = page * limit;
		return await this.userModel.find(filter, '').limit(limit).skip(skip).lean() as any;
	}

	public async countAllUser(filter: any): Promise<number> {

		return await this.userModel.countDocuments(filter);
	}

	public async getUserById(userId: String): Promise<UserCreateDTO> {
		const user = await this.userModel.findById(userId);
		return user;
	}

	public async findUserByEmailOrMobile(email: string, mobileNumber: string): Promise<UserCreateDTO> {
		if (email) email = email.toLowerCase();
		const user = await this.userModel.findOne({ $or: [{ email: email }, { mobileNumber: mobileNumber }] });
		return user;
	}

	public async getAllUserList(page: number, limit: number): Promise<Array<AdminUserDTO>> {
		const skip = page * limit;
		let filter = { role: UserRoles.USER};
		const resData = await this.userModel.find(filter, 'userName email mobileNumber address').sort({createdAt:-1}).limit(limit).skip(skip);
		return resData
	}

	public async countAllUserList(): Promise<number> {
		let filter = { role: UserRoles.USER };
		const count = await this.userModel.countDocuments(filter);
		return count;
	}

	public async deleteUserById(userId: String): Promise<UserCreateDTO> {
		const user = await this.userModel.findByIdAndRemove(userId);
		return user;
	}
	
	public convertToNumber(input: string): number {
		var number = Number(input);
		if (!isNaN(number)) {
			return number;
		} else return 0;
	}

	public getAdminPagination(query: AdminQuery) {
		return {
			page: Number(query.page) || AdminSettings.DEFAULT_PAGE_NUMBER,
			limit: Number(query.limit) || AdminSettings.DEFAULT_PAGE_LIMIT,
	
		}
	}
    
	public getUserQuery(query: UserQuery) {
		return {
			page: Number(query.page) || AdminSettings.DEFAULT_PAGE_NUMBER,
			limit: Number(query.limit) || AdminSettings.DEFAULT_PAGE_LIMIT,
		};
	}
    
	public async getAllUserListByLocation(page: number, limit: number, lngLat): Promise<Array<any>> {
		const skip = (page - 1) * limit;
		let filter = {}
		// let  data = await this.userModel.find({"address.location": {
		// 	"$geoNear": {
		// 		"$near": [
		// 			lngLat,
		// 			0.001
		// 		 ]
		// 	}
		// }})
		// console.log("bbbbbbb",data)
		 let data = await this.userModel.aggregate([
			{
				$geoNear: {
					near: { type: "Point", coordinates: lngLat },
					spherical: true,
					query: filter,
					distanceField: "calcDistance"
				},
			},
			{ $project: { userName: 1, email: 1, mobileNumber: 1, address: 1} },
			{ $sort: { "calcDistance": 1 } },
			{ $limit: limit },
			{ $skip: skip },

		])
		return data
	}

}
