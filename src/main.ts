import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { UsersModule } from './users/users.module';

import * as dotenv from 'dotenv';
import * as sentry from '@sentry/node';

const os = require('os');
import * as bodyParser from 'body-parser';


async function bootstrap() {
	dotenv.config();
	const app = await NestFactory.create(AppModule, {
		bodyParser: false
	});

	const rawBodyBuffer = (req, res, buf, encoding) => {
		if (buf && buf.length) {
			req.rawBody = buf.toString(encoding || 'utf8');
		}
	};

	app.use(bodyParser.urlencoded({ verify: rawBodyBuffer, extended: true }));
	app.use(bodyParser.json({ verify: rawBodyBuffer }));
	app.useGlobalPipes(new ValidationPipe());
	app.enableCors();

	app.use((req, res, next) => {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
		res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
		next();
	});

	if (process.env.NODE_ENV === 'production' && process.env.SENTRY_URL) sentry.init({ dsn: process.env.SENTRY_URL });

	if (process.env.PREDIFINED && process.env.PREDIFINED == "true") {
		let options = new DocumentBuilder().setTitle('Mounty App').setBasePath("/").setVersion('v1').addBearerAuth().setSchemes('https', 'http').build();

		const document = SwaggerModule.createDocument(app, options, {
			include: [ UsersModule]
		});
		SwaggerModule.setup('/explorer', app, document);
	}

	const port = process.env.PORT || 5000;
	await app.listen(port);
	console.log(`http://localhost:${port}/explorer/#/`)
}

bootstrap();
